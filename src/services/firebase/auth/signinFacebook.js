import firebase from '../firebase.js'

export default function () {
  const provider = new firebase.auth.FacebookAuthProvider()
  firebase.auth().signInWithRedirect(provider)
}
