import database from './database.js'

export default argument => {
  const refArgument = database.ref('arguments').push()
  argument.uid = refArgument.key
  return refArgument.set(argument)
}
