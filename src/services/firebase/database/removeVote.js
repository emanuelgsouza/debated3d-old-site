import { transactionFavor, transactionNotFavor } from './index.js'
import { removeUid } from '../../../helpers'

export default (vote, uid, card, userVoted) => {
  if (vote === 'dislike') {
    transactionFavor(uid)
      .transaction(currentData => {
        if (currentData !== null) {
          return removeUid(currentData, userVoted)
        }
      }, (err, bool, snap) => {
        if (err) console.log(err)
        if (bool) card.favorVotes = snap.val()
      })
  } else {
    transactionNotFavor(uid)
      .transaction(currentData => {
        if (currentData !== null) {
          return removeUid(currentData, userVoted)
        }
      }, (err, bool, snap) => {
        if (err) console.log(err)
        if (bool) card.notFavorVotes = snap.val()
      })
  }
  return true
}
