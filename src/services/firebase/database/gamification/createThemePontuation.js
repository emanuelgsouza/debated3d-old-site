import database from '../database'

export default uid => {
  const refUser = database.ref('users').child(uid)
  return [
    refUser.child('ponts').transaction(val => val + 10)
  ]
}
