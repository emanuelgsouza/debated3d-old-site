import database from '../database'

export default (uid, uidOtherUser, action, favorited) => {
  console.log(favorited)
  const refUser = database.ref('users').child(uid)
  const refOtherUser = database.ref('users').child(uidOtherUser)
  const arr = []
  if (!favorited) arr.push(refUser.child('ponts').transaction(val => val + 2))
  if (favorited) arr.push(refUser.child('ponts').transaction(val => val - 2))
  if (action === 'add' && !favorited) {
    arr.push(refOtherUser.child('ponts').transaction(val => val + 10))
  }
  if (action === 'remove' && favorited) {
    arr.push(refOtherUser.child('ponts').transaction(val => val - 10))
  }
  return arr
}
