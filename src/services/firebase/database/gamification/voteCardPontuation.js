import database from '../database'

export default (uid, uidOtherUser, action, voted) => {
  const refUser = database.ref('users').child(uid)
  const refOtherUser = database.ref('users').child(uidOtherUser)
  const arr = []

  // Estou votando pela primeira vez
  if (action === 'like' && !voted) {
    arr.push(refUser.child('ponts').transaction(val => val + 2))
    arr.push(refOtherUser.child('ponts').transaction(val => val + 5))
  }
  if (action === 'dislike' && !voted) {
    arr.push(refUser.child('ponts').transaction(val => val + 2))
    arr.push(refOtherUser.child('ponts').transaction(val => (val === 0) ? 0 : val - 1))
  }

  // Não estou votando pela primeira vez
  if (action !== 'like' && voted) {
    arr.push(refOtherUser.child('ponts').transaction(val => val - 6))
  }
  if (action !== 'dislike' && voted) {
    arr.push(refOtherUser.child('ponts').transaction(val => val + 6))
  }
  return arr
}
