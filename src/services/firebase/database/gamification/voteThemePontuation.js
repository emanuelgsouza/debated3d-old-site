import database from '../database'

export default (uid, uidOtherUser) => {
  const refUser = database.ref('users').child(uid)
  const refOtherUser = database.ref('users').child(uidOtherUser)
  return [
    refUser.child('ponts').transaction(val => val + 2),
    refOtherUser.child('ponts').transaction(val => val + 1)
  ]
}
