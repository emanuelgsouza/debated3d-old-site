import database from './database'

export default uid => {
  return database
    .ref(`arguments/${uid}/edited`)
    .set(true)
}
