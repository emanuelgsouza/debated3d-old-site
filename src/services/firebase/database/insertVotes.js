import database from './database.js'
import store from '../../../store'

export default (position, uidArgument) => {
  const uidUser = store.state.user.uid
  const user = store.state.user
  if (user.votes === undefined) {
    user.votes = {}
  }
  user.votes[uidArgument] = {
    position
  }
  return database
      .ref('users/')
      .child(uidUser)
      .set(user)
}
