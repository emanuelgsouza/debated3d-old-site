import { transactionFavorite } from './index.js'
import { isFavorited, removeUid } from '../../../helpers'

export default (uid, card, userVoted) => {
  if (isFavorited(uid)) return null
  transactionFavorite(uid)
    .transaction(currentData => {
      if (currentData !== null) return removeUid(currentData, userVoted)
    }, (err, bool, snap) => {
      if (err) console.log(err)
      if (bool) card.favorites = snap.val()
    })
  return true
}
