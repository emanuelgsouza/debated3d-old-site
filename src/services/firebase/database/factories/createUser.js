export default (user, created) => {
  return {
    name: user.displayName,
    uid: user.uid,
    photo: user.photoURL,
    email: user.email,
    created,
    isSubscriber: false,
    deck: [],
    arguments: [],
    ponts: 0,
    isVerified: false,
    cep: '',
    cpf: '',
    shared: {}
  }
}
