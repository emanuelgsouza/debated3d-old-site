import database from './database'

export default obj => {
  return database.ref(`themes/${obj.uid}`).set(obj)
}
