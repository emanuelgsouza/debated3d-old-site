import database from './database'

export default (uidArgument, uidAuthor, uidTheme) => {
  const updates = {}
  updates[`users/${uidAuthor}/arguments/${uidTheme}/${uidArgument}`] = uidArgument
  updates[`themes/${uidTheme}/arguments/${uidArgument}`] = uidArgument
  return database.ref().update(updates)
}
