import database from './database'

export default (uidTheme, limit) => {
  return database
    .ref('arguments/')
    .orderByChild('uidTheme')
    .equalTo(uidTheme)
    .limitToFirst(limit)
}
