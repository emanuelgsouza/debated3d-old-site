import database from './database.js'

export default uidTheme => {
  return database
    .ref('arguments/')
    .orderByChild('uidTheme')
    .equalTo(uidTheme)
}
