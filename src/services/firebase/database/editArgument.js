import database from './database'

export default (uid, content) => {
  console.log(content)
  return database
    .ref(`arguments/${uid}/content`)
    .set(content)
}
