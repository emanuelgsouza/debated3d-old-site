import database from './database.js'

export default (vote, theme, uidTheme) => {
  const refTheme = database.ref('themes').child(uidTheme)
  if (vote) {
    refTheme
      .child('notFavorVotes')
      .transaction(currentData => {
        return currentData - 1
      }, (err, bool, snap) => {
        if (err) console.log(err)
        if (bool) theme.favorVotes = snap.val()
      })
  } else {
    refTheme
      .child('favorVotes')
      .transaction(currentData => {
        return currentData - 1
      }, (err, bool, snap) => {
        if (err) console.log(err)
        if (bool) theme.notFavorVotes = snap.val()
      })
  }
  return true
}
