import database from './database.js'

export default uidTheme =>
  database.ref('arguments/').orderByChild('uidTheme').equalTo(uidTheme)
