import database from './database.js'
import store from '../../../store'

export default (idTheme, position) => {
  const user = store.state.user
  if (user.voteThemes === undefined) {
    user.voteThemes = {
      idTheme: position
    }
  }
  user.voteThemes[idTheme] = position
  return database
      .ref('users')
      .child(user.uid)
      .set(user)
}
