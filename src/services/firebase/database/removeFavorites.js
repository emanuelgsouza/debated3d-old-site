import database from './database.js'
import store from '../../../store'

export default (uidArgument, uidTheme) => {
  const uidUser = store.state.user.uid
  const user = store.state.user
  if (user.deck === undefined) return null
  if (user.deck[uidTheme] === undefined) return null
  if (user.deck[uidTheme][uidArgument] === undefined) return null
  delete user.deck[uidTheme][uidArgument]
  if (Object.keys(user.deck).length === 1) {
    store.dispatch('addDeck', {})
  }
  return database
      .ref('users/')
      .child(uidUser)
      .set(user)
}
