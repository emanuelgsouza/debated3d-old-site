import database from './database.js'
import insertVoteTheme from './insertVoteTheme'

export default (uidTheme, vote, theme) => {
  const refTheme = database.ref('themes').child(uidTheme)
  if (vote === 1) {
    refTheme.child('favorVotes')
      .transaction(currentData => {
        if (currentData === null) return 1
        return currentData + 1
      }, (err, bool, snap) => {
        if (err) console.log(err)
        if (bool) theme.favorVotes = snap.val()
        insertVoteTheme(uidTheme, vote)
      })
  }
  if (vote === 0) {
    refTheme.child('notFavorVotes')
      .transaction(currentData => {
        if (currentData === null) return 1
        return currentData + 1
      }, (err, bool, snap) => {
        if (err) console.log(err)
        if (bool) theme.notFavorVotes = snap.val()
        insertVoteTheme(uidTheme, vote)
      })
  }
}
