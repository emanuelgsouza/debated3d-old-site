import database from './database.js'
import store from '../../../store'

export default (uidTheme, uidArgument) => {
  const userUid = store.state.user.uid
  const updates = {}
  updates[`users/${userUid}/deck/${uidTheme}/${uidArgument}`] = uidArgument
  return database.ref().update(updates)
}
