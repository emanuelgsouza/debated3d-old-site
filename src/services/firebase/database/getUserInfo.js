import database from './database'

export default uid => database.ref('users').child(uid).once('value')
