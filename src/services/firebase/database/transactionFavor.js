import database from './database.js'

export default (uidArgument) => {
  return database
    .ref('arguments/')
    .child(uidArgument)
    .child('favorVotes')
}
