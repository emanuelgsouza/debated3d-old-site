import { transactionFavor, transactionNotFavor } from './index.js'
import { isVoted } from '../../../helpers'

export default (vote, uid, card, userVoted) => {
  if (isVoted(uid) === vote) return null
  if (vote === 'like') {
    transactionFavor(uid)
      .transaction(currentData => {
        if (currentData === null) return new Array(userVoted)
        currentData.push(userVoted)
        return currentData
      }, (err, bool, snap) => {
        if (err) console.log(err)
        if (bool) card.favorVotes = snap.val()
      })
  } else {
    transactionNotFavor(uid)
      .transaction(currentData => {
        if (currentData === null) return new Array(userVoted)
        currentData.push(userVoted)
        return currentData
      }, (err, bool, snap) => {
        if (err) console.log(err)
        if (bool) card.notFavorVotes = snap.val()
      })
  }
  return true
}
