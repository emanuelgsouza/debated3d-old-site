import database from './database'
import checkExistUser from './checkExistUser'
import { createUser } from './factories'
import { setUserStorage, setHasLogged } from '../../../helpers'

export default (user, store) => {
  checkExistUser(user.uid)
    .then(data => {
      if (data.val()) {
        database
          .ref('users')
          .child(user.uid)
          .on('value', function (data) {
            store.dispatch('setUser', data.val())
            setUserStorage(data.val())
            setHasLogged(true)
            console.log('User storage in localStorage')
          })
        return
      }
      const created = new Date().getTime()
      const usuario = createUser(user, created)
      database
        .ref(`users/${usuario.uid}`)
        .set(usuario)
        .then(() => {
          store.dispatch('setUser', usuario)
          setHasLogged(true)
          console.log('User insert in database success and storage in localStorage')
        })
    })
}
