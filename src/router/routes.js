import Home from '../views/Home'
import Card from '../views/Card'
import Profile from '../views/Profile'
import CreateTheme from '../views/CreateTheme'
import ThemeDashboard from '../views/ThemeDashboard'
import Subscriber from '../views/Subscriber'
import ListThemeForCategory from '../views/ListThemeForCategory'
import Deck from '../views/Deck'
import MyArguments from '../views/MyArguments'
import ListThemes from '../views/ListThemes'
import Policy from '../views/Policy'
import Ranking from '../views/Ranking'
import EditedProfile from '../views/EditedProfile'

export default [
  {
    path: '/',
    redirect: '/home',
    meta: { requireAuth: false }
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta: { requireAuth: false }
  },
  {
    path: '/categories/:category',
    name: 'ListThemeForCategory',
    component: ListThemeForCategory,
    meta: { requireAuth: false }
  },
  {
    path: '/themes',
    name: 'ListThemes',
    component: ListThemes,
    meta: { requireAuth: false }
  },
  {
    path: '/themes/:theme',
    name: 'Themes',
    component: ThemeDashboard,
    meta: { requireAuth: false }
  },
  {
    path: '/card/:card',
    name: 'Card',
    component: Card,
    meta: { requireAuth: false }
  },
  {
    path: '/create-theme',
    name: 'CreateTheme',
    component: CreateTheme,
    meta: { requireAuth: true }
  },
  {
    path: '/user/:user',
    name: 'ShowProfile',
    component: Profile,
    meta: { requireAuth: false }
  },
  {
    path: '/edited',
    name: 'EditedProfile',
    component: EditedProfile,
    meta: { requireAuth: true }
  },
  {
    path: '/subscriber',
    name: 'Subscriber',
    component: Subscriber,
    meta: { requireAuth: true }
  },
  {
    path: '/my-arguments',
    name: 'My Arguments',
    component: MyArguments,
    meta: { requireAuth: true }
  },
  {
    path: '/ranking',
    name: 'Users ranking',
    component: Ranking
  },
  {
    path: '/deck-arguments',
    name: 'Deck Arguments',
    component: Deck,
    meta: { requireAuth: true }
  },
  {
    path: '/terms-of-use',
    name: 'Policy',
    component: Policy,
    meta: { requireAuth: false }
  }
]
