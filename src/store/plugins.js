import { firebase } from '../services/firebase'
import { setUser, database } from '../services/firebase/database'
import { getUserStorage, isLogged } from '../helpers'

const initializeApplication = store => {
  const dispatchAction = (data, action) => {
    if (!data.val()) return
    store.dispatch(action, data.val())
  }

  window.setTimeout(() => {
    database
      .ref('themes')
      .on('value', function addTheme (data) {
        dispatchAction(data, 'addTheme')
      })
  }, 2000)

  firebase.auth().onAuthStateChanged(function setuser (user) {
    if (!user) return
    if (isLogged()) store.dispatch('setUser', getUserStorage())
    setUser(user, store)
    const referenceUser = database.ref('users').child(user.uid)

    database
      .ref('arguments/')
      .orderByChild('uidAuthor')
      .equalTo(user.uid)
      .on('value', function addArgument (data) {
        dispatchAction(data, 'addArguments')
      })

    referenceUser
      .child('deck')
      .on('value', function addArgument (data) {
        dispatchAction(data, 'addDeck')
      })
  })
}
export default [ initializeApplication ]
