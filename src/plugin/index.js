// Import filters
import * as filters from './filters'

// Import directives
import * as directives from './directives'

// Import components
import * as components from '../components'

import * as helpers from './helpers'

const setProperties = (Vue, object, property) => {
  Object.keys(object).forEach(key => Vue[property](key, object[key]))
}

console.log(helpers)

const myPlugin = {
  install (Vue, options) {
    // helpers
    Vue.prototype.$isEmpty = val => helpers['isEmpty'](val)

    // Filters
    setProperties(Vue, filters, 'filter')

    // Directives
    setProperties(Vue, directives, 'directive')

    // Global Components
    setProperties(Vue, components, 'component')
  }
}

export default myPlugin
