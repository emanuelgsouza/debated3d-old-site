import { default as empty } from 'lodash.isempty'

export const isEmpty = val => empty(val)
