import { returnUid } from '../helpers'

export const returnLenght = value => (!value) ? 0 : value.length

export const capitalize = value => value.toUpperCase()

export const returnUidFilter = value => returnUid(value)
