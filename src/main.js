// Imports
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import 'assets/sass/app.sass'
import Vuefire from 'vuefire'
import myPlugin from './plugin'
import vueMoment from 'vue-moment'
import moment from 'moment'
import SocialSharing from 'vue-social-sharing'
import vueHead from 'vue-head'
import VueTheMask from 'vue-the-mask'

moment.locale('pt-BR')

// Plugins
Vue.use(Vuefire)
Vue.use(myPlugin)
Vue.use(vueMoment, { moment })
Vue.use(SocialSharing)
Vue.use(VueTheMask)
Vue.use(vueHead, {
  separator: '-',
  complement: 'Debate3D'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
