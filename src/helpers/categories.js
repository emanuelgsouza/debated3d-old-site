export default [
  {
    name: 'education',
    namePt: 'Educação',
    color: '#f1c40f',
    description: 'Os melhores temas para discussão em sala de aula estão aqui. Teorias, ideologias, bioética e muito mais... Sua aula nunca mais será a mesma'
  },
  {
    name: 'policy',
    namePt: 'Política',
    color: '#2ecc71',
    description: 'Direita x Esquerda; Coxinha x Mortadela; Capitalismo x Socialista e muita tensão e polêmica de tirar o fôlego'
  },
  {
    name: 'religion',
    namePt: 'Religião',
    color: '#9b59b6',
    description: 'Teísmo, ateísmo, deísmo, agnoticismo, ceticismo e valores da vida... Temas para uma geração que acredita ou não em alguma coisa'
  }
]
