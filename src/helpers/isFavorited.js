import store from '../store'
import isMyArgument from './isMyArgument'

export default (uidTheme, uidArgument) => {
  if (isMyArgument(uidArgument)) return
  const deck = store.state.deck
  if (deck === undefined) return false
  if (deck[uidTheme] === undefined) return false
  if (deck[uidTheme][uidArgument] === undefined) return false
  return true
}
