import remove from 'lodash.remove'

export default (currentData, value) => {
  return remove(currentData, item => item !== value)
}
