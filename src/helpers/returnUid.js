import capitalize from 'lodash.capitalize'

export default value => {
  const theme = value.split('-')
  const string = theme.map(index => capitalize(index))
  return string.join(' ')
}
