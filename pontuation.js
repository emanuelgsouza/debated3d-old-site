/*

********************

Script para geração dos pontos do usuário. Tal script é essencial para podermos gerar automaticamente os pontos do usuário, sem a necessidade de fazer a mão

********************

*/

const db = require('./debate-bd.json')
const fs = require('fs')

const argumentos = Object.values(db.arguments)

const usersDb = db.users

const themes = Object.values(db.themes)

const user = {}

argumentos.forEach(function (obj) {
  let ponts = 0
  if (obj.favorVotes !== undefined) {
    ponts += obj.favorVotes.length * 5
  }
  if (obj.notFavorVotes !== undefined) {
    ponts -= obj.notFavorVotes.length
  }
  if (obj.favorites !== undefined) {
    ponts += obj.favorites.length * 10
  }
  if (user[obj.uidAuthor] === undefined) {
    user[obj.uidAuthor] = {
      name: obj.uidAuthor,
      ponts: 0
    }
  }
  if (user[obj.uidAuthor]['ponts'] !== undefined) {
    user[obj.uidAuthor]['ponts'] += ponts
  } else {
    user[obj.uidAuthor]['ponts'] = ponts
  }
})

themes.forEach(function (obj) {
  let ponts = 0
  if (obj.favorVotes !== undefined) {
    ponts += obj.favorVotes
  }
  if (obj.notFavorVotes !== undefined) {
    ponts += obj.notFavorVotes
  }
  if (obj.arguments !== undefined) {
    ponts += Object.values(obj.arguments).length * 5
  }
  if (user[obj.author] === undefined) {
    user[obj.author] = {
      name: obj.author,
      ponts: 0
    }
  }
  if (user[obj.author]['ponts'] !== undefined) {
    user[obj.author]['ponts'] += ponts
  } else {
    user[obj.author]['ponts'] = ponts
  }
  console.log(user[obj.author])
})

Object.values(usersDb).forEach(function (obj) {
  let ponts = 0
  if (obj.votes !== undefined) {
    ponts += Object.values(obj.votes).length * 2
  }
  if (obj.votesTheme !== undefined) {
    ponts += Object.values(obj.votesTheme).length * 5
  }
  if (obj.arguments !== undefined) {
    ponts += Object.values(obj.arguments).length * 10
  }
  if (obj.deck !== undefined) {
    ponts += Object.values(obj.deck).length * 2
  }
  if (user[obj.uid] === undefined) {
    user[obj.uid] = {
      name: obj.uid,
      ponts
    }
  }
  if (user[obj.uid]['ponts'] !== undefined) {
    user[obj.uid]['ponts'] += ponts
  }
  obj.ponts = user[obj.uid]['ponts']
})

fs.writeFile('./users.json', JSON.stringify(usersDb), err => {
  if (err) throw err
})
